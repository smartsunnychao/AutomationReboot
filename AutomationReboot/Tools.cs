﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutomationReboot
{
    public static class Tools
    {
        public static void RebootWindows()
        {
            DoExitWin(WinApi.ExitWindows.Reboot);
            //DoExitWin(WinApi.ExitWindows.Reboot | WinApi.ExitWindows.Force);
        }

        private static void DoExitWin(WinApi.ExitWindows flags)
        {
            bool ok;
            WinApi.TokPriv1Luid tp;
            IntPtr hproc = WinApi.GetCurrentProcess();
            IntPtr htok = IntPtr.Zero;

            ok = WinApi.OpenProcessToken(hproc, WinApi.TOKEN_ADJUST_PRIVILEGES | WinApi.TOKEN_QUERY, ref htok);
            tp.Count = 1;
            tp.Luid = 0;
            tp.Attr = WinApi.SE_PRIVILEGE_ENABLED;
            ok = WinApi.LookupPrivilegeValue(null, WinApi.SE_SHUTDOWN_NAME, ref tp.Luid);
            ok = WinApi.AdjustTokenPrivileges(htok, false, ref tp, 0, IntPtr.Zero, IntPtr.Zero);
            ok = WinApi.ExitWindowsEx(flags, WinApi.ShutdownReason.MajorOther);
        }
    }
}
