﻿namespace AutomationReboot
{
    partial class RebootForm
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnRestart = new System.Windows.Forms.Button();
            this.tmrReboot = new System.Windows.Forms.Timer(this.components);
            this.btnResume = new System.Windows.Forms.Button();
            this.lblRebootTime = new System.Windows.Forms.Label();
            this.tbRebootTime = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbNowTime = new System.Windows.Forms.TextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.nico = new System.Windows.Forms.NotifyIcon(this.components);
            this.SuspendLayout();
            // 
            // btnRestart
            // 
            this.btnRestart.Location = new System.Drawing.Point(140, 12);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(80, 23);
            this.btnRestart.TabIndex = 0;
            this.btnRestart.Text = "Restart";
            this.btnRestart.UseVisualStyleBackColor = true;
            this.btnRestart.Click += new System.EventHandler(this.btnRestart_Click);
            // 
            // tmrReboot
            // 
            this.tmrReboot.Enabled = true;
            this.tmrReboot.Tick += new System.EventHandler(this.tmrReboot_Tick);
            // 
            // btnResume
            // 
            this.btnResume.Location = new System.Drawing.Point(25, 12);
            this.btnResume.Name = "btnResume";
            this.btnResume.Size = new System.Drawing.Size(75, 23);
            this.btnResume.TabIndex = 1;
            this.btnResume.Text = "Pause";
            this.btnResume.UseVisualStyleBackColor = true;
            this.btnResume.Click += new System.EventHandler(this.btnResume_Click);
            // 
            // lblRebootTime
            // 
            this.lblRebootTime.AutoSize = true;
            this.lblRebootTime.Location = new System.Drawing.Point(12, 44);
            this.lblRebootTime.Name = "lblRebootTime";
            this.lblRebootTime.Size = new System.Drawing.Size(69, 12);
            this.lblRebootTime.TabIndex = 2;
            this.lblRebootTime.Text = "Reboot Time:";
            // 
            // tbRebootTime
            // 
            this.tbRebootTime.BackColor = System.Drawing.SystemColors.Window;
            this.tbRebootTime.Location = new System.Drawing.Point(106, 41);
            this.tbRebootTime.Name = "tbRebootTime";
            this.tbRebootTime.ReadOnly = true;
            this.tbRebootTime.Size = new System.Drawing.Size(114, 22);
            this.tbRebootTime.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "Now Time:";
            // 
            // tbNowTime
            // 
            this.tbNowTime.BackColor = System.Drawing.SystemColors.Window;
            this.tbNowTime.Location = new System.Drawing.Point(106, 68);
            this.tbNowTime.Name = "tbNowTime";
            this.tbNowTime.ReadOnly = true;
            this.tbNowTime.Size = new System.Drawing.Size(114, 22);
            this.tbNowTime.TabIndex = 5;
            // 
            // lblStatus
            // 
            this.lblStatus.BackColor = System.Drawing.Color.Gainsboro;
            this.lblStatus.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblStatus.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblStatus.Location = new System.Drawing.Point(92, 93);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(128, 27);
            this.lblStatus.TabIndex = 6;
            this.lblStatus.Text = "Waiting Reboot...";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // nico
            // 
            this.nico.Text = "notifyIcon1";
            this.nico.Visible = true;
            this.nico.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.nico_MouseDoubleClick);
            // 
            // RebootForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 129);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.tbNowTime);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbRebootTime);
            this.Controls.Add(this.lblRebootTime);
            this.Controls.Add(this.btnResume);
            this.Controls.Add(this.btnRestart);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "RebootForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Automation Reboot ";
            this.Load += new System.EventHandler(this.frmReboot_Load);
            this.Resize += new System.EventHandler(this.RebootForm_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRestart;
        private System.Windows.Forms.Timer tmrReboot;
        private System.Windows.Forms.Button btnResume;
        private System.Windows.Forms.Label lblRebootTime;
        private System.Windows.Forms.TextBox tbRebootTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbNowTime;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.NotifyIcon nico;
    }
}

