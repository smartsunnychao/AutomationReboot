﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AutomationReboot
{
    public partial class RebootForm : Form
    {
        private enum RebootStatus
        {
            Pause = 1,
            Resume = 2
        }

        private RebootStatus m_currentStatus;
        private DateTime m_nextRebootTime;
        private static string DateTimeFormat = "yyyy/MM/dd HH:mm:ss";

        public RebootForm()
        {
            InitializeComponent();
        }

        private void Restart()
        {
            m_currentStatus = RebootStatus.Resume;
            btnResume.Text = "Pause";
            lblStatus.Text = "Waiting Reboot...";

            DateTime now = DateTime.Now;

            m_nextRebootTime = now.AddHours(12);
            tbNowTime.Text = now.ToString(DateTimeFormat);
            tbRebootTime.Text = m_nextRebootTime.ToString(DateTimeFormat);
        }

        private void frmReboot_Load(object sender, EventArgs e)
        {  
            this.WindowState = FormWindowState.Minimized;
            nico.Icon = AutomationReboot.Properties.Resources.RebootIcon;
            nico.Visible = true;
            Restart();
        }

        private void btnRestart_Click(object sender, EventArgs e)
        {
            Restart();
        }

        private void btnResume_Click(object sender, EventArgs e)
        {
            if (m_currentStatus == RebootStatus.Resume)
            {
                m_currentStatus = RebootStatus.Pause;
                btnResume.Text = "Resume";
                lblStatus.Text = "Pause";
            }
            else
            {
                m_currentStatus = RebootStatus.Resume;
                btnResume.Text = "Pause";
                lblStatus.Text = "Waiting Reboot...";
            }
        }

        private void tmrReboot_Tick(object sender, EventArgs e)
        {
            DateTime now = DateTime.Now;

            tbNowTime.Text = now.ToString(DateTimeFormat);

            if (m_currentStatus == RebootStatus.Resume)
            {
                if (now > m_nextRebootTime)
                    Tools.RebootWindows();
            }
        }

        private void nico_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            nico.Visible = false;
        }

        private void RebootForm_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.ShowInTaskbar = false;
                this.Visible = false;
                nico.Visible = true;
            }
            else
            {
                this.Visible = true;
                nico.Visible = false;
            }
        }
    }
}
